![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

A small browser tool to check grammatical tenses, based on [compromise.js](https://github.com/spencermountain/compromise)

The program can be found here: https://gitlab.com/Elmosfire/tenses