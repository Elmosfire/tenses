class Cursor {
    static getCurrentCursorPosition(parentElement) {
        var selection = window.getSelection(),
            charCount = -1,
            node;
        
        if (selection.focusNode) {
            if (Cursor._isChildOf(selection.focusNode, parentElement)) {
                node = selection.focusNode; 
                charCount = selection.focusOffset;
                
                while (node) {
                    if (node === parentElement) {
                        break;
                    }

                    if (node.previousSibling) {
                        node = node.previousSibling;
                        charCount += node.textContent.length;
                        if (node.nodeName.toLowerCase() == "br") {
                            charCount += 1
                        }
                    } else {
                        node = node.parentNode;
                        if (node === null) {
                            break;
                        }
                    }
                }
            }
        }
        
        return charCount;
    }
    
    static setCurrentCursorPosition(chars, element) {
        if (chars >= 0) {
            var selection = window.getSelection();
            
            let range = Cursor._createRange(element, { count: chars });

            if (range) {
                range.collapse(false);
                selection.removeAllRanges();
                selection.addRange(range);
            }
        }
    }
    
    static _createRange(node, chars, range) {
        if (!range) {
            range = document.createRange()
            range.selectNode(node);
            range.setStart(node, 0);
        }

        if (chars.count === 0) {
            range.setEnd(node, chars.count);
        } else if (node && chars.count >0) {
            if (node.nodeType === Node.TEXT_NODE) {
                if (node.textContent.length < chars.count) {
                    chars.count -= node.textContent.length;
                } else {
                    range.setEnd(node, chars.count);
                    chars.count = 0;
                }
            } else if (node.nodeName.toLowerCase() == "br") {
                if (1 < chars.count) {
                    chars.count -= 1;
                } else {
                    range.setEnd(node.nextSibling, 0);
                    chars.count = 0;
                }
            } else {
                for (var lp = 0; lp < node.childNodes.length; lp++) {
                    range = Cursor._createRange(node.childNodes[lp], chars, range);

                    if (chars.count === 0) {
                    break;
                    }
                }
            }
        } 

        return range;
    }
    
    static _isChildOf(node, parentElement) {
        while (node !== null) {
            if (node === parentElement) {
                return true;
            }
            node = node.parentNode;
        }

        return false;
    }
}

const editor = document.getElementById('editor');
const selectionOutput = document.getElementById('selection');
let corrpos = null;

function getTextSegments(element) {
    const textSegments = [];
    Array.from(element.childNodes).forEach((node) => {
        if (node.nodeName.toLowerCase() == "br") {
            textSegments.push({text: "\n", node});
        }
        switch(node.nodeType) {
            case Node.TEXT_NODE:
                // console.log("text node " + node.nodeName)
                textSegments.push({text: node.nodeValue, node});
                break;
                
            case Node.ELEMENT_NODE:
                // console.log("element node " + node.nodeName)
                if (!node.classList.contains("uneditable"))
                {
                textSegments.splice(textSegments.length, 0, ...(getTextSegments(node)));
                }
                break;
                
            default:
                throw new Error(`Unexpected node type: ${node.nodeType}`);
        }
    });
    return textSegments;
}

document.addEventListener('keydown', event => {
    if (event.key === 'Enter') {
      corrpos = Cursor.getCurrentCursorPosition(editor);
      console.log("corr: " + corrpos)
      document.execCommand('insertLineBreak')
      event.preventDefault()
    }
  })

editor.addEventListener('input', updateEditor);

function updateEditor() {
    let offset = Cursor.getCurrentCursorPosition(editor);
    if (corrpos !== null) {
        console.log("correcting pos")
        offset = corrpos + 2;
        corrpos = null;
    }
    const textSegments = getTextSegments(editor);
    const textContent = textSegments.map(({text}) => text).join('');
    
    editor.innerHTML = renderText(textContent);

    Cursor.setCurrentCursorPosition(offset, editor);
    editor.focus();
    
}

function escape(text) {
    return text.replace(/ +/g, "&#32;")
}

function renderSentence(tokens) {
    res = tokens.terms.map(element => {
        console.log(element.chunk)
        if (element.chunk == 'Verb') {
            let tags = element.tags;
            let class_ = "verbgeneral";
            if (tags.includes('PresentTense')) {
                class_ = "verbpresent"
            }
            if (tags.includes('PastTense')) {
                class_ = "verbpast"
            }
            if (tags.includes('PerfectTense')) {
                class_ = "verbperfect"
            }
            if (tags.includes('FuturePerfect')) {
                class_ = "verbfuture"
            }
            return escape(element.pre) + "<span class=\"" + class_ + "\">" + escape(element.text) + "</span>" + escape(element.post)
        }
        else
        {
            return escape(element.pre + element.text + element.post)
        }
    });
    return res.join('')
}

function renderParagraph(text) {
    var doc = nlp(text);
    if (doc.json()[0] == undefined) {
        return text
    }
    let tokens = doc.json();
    res = tokens.map(renderSentence);
    return res.join('') + "&nbsp;"
}

function renderText(text) {
    res = text.split("\n").map(renderParagraph)
    new_text = res.join('\n')
    
    new_text = new_text.replace(/(?:\r\n|\r|\n)/g, "<br>")
    return new_text;
}

updateEditor();

